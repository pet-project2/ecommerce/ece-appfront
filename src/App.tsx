import './App.css'
import Header from "./shared/Header/Header.tsx";
import Register from "./components/Register/Register.tsx";

function App() {

  return (
    <>
        <Header/>
        <Register/>
    </>
  )
}

export default App
