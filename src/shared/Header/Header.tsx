import React from "react";
import templateLogo from '../../assets/images/template-white-logo.png';

const Header: React.FC = () => {
    return (
        <>
            <header className="bg-gray-dark sticky top-0 z-50">
                <div className="container mx-auto flex justify-between items-center py-4">
                    <a href="index.html" className="flex items-center">
                        <div>
                            <img src={templateLogo as string} alt="Logo" className="h-14 w-auto mr-4"/>
                        </div>
                    </a>

                    {/*<div className="flex lg:hidden">*/}
                    {/*    <button id="hamburger" className="text-white focus:outline-none">*/}
                    {/*        <svg className="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">*/}
                    {/*            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"*/}
                    {/*                  d="M4 6h16M4 12h16m-7 6h7"></path>*/}
                    {/*        </svg>*/}
                    {/*    </button>*/}
                    {/*</div>*/}

                    {/*<nav className="hidden lg:flex md:flex-grow justify-center">*/}
                    {/*    <ul className="flex justify-center space-x-4 text-white">*/}
                    {/*        <li><a href="index.html" className="hover:text-secondary font-semibold">Home</a></li>*/}
                    {/*        <li className="relative group" x-data="{ open: false }">*/}
                    {/*            <a href="#" className="hover:text-secondary font-semibold flex items-center">*/}
                    {/*                <i className="fas fa-chevron-down ml-1 text-xs"></i>*/}
                    {/*            </a>*/}
                    {/*            <ul className="absolute left-0 bg-white text-black space-y-2 mt-1 p-2 rounded shadow-lg">*/}
                    {/*                <li><a href="shop.html"*/}
                    {/*                       className="min-w-40 block px-4 py-2 hover:bg-primary hover:text-white rounded">Men*/}
                    {/*                    Item 1</a></li>*/}
                    {/*                <li><a href="shop.html"*/}
                    {/*                       className="min-w-40 block px-4 py-2 hover:bg-primary hover:text-white rounded">Men*/}
                    {/*                    Item 2</a></li>*/}
                    {/*                <li><a href="shop.html"*/}
                    {/*                       className="min-w-40 block px-4 py-2 hover:bg-primary hover:text-white rounded">Men*/}
                    {/*                    Item 3</a></li>*/}
                    {/*            </ul>*/}
                    {/*        </li>*/}
                    {/*    </ul>*/}
                    {/*</nav>*/}

                </div>
            </header>
        </>
    )
}

export default Header;
